'use strict';

const getData = function () {
    return [
        {"name": "Batteries", "alternate": "household hazardous waste, HHW, double AA, car battery, alkaline, rechargeable, ni-cad, lithium ion, lead acid, hearing aid battery AAA, D, C, single use battery, dry cell, wet cell"},
        {"name": "Bicycles","alternate": "household goods, bike, kid bike, mountain bike, tricycle, unicycle"},
        {"name": "Books", "alternate": "household goods, hard back book, soft cover book, textbook, encyclopedia, school book, children book"},
        {"name": "Car Seats", "alternate":"household goods, infant seat, baby seat, booster, child seat"},
        {"name":"Cassette & VCR Tapes", "alternate": "tangler, VCR tape, film, movies, videocassette"},
        {"name":"Clothing & Household Goods","alternate": "household goods, tangler, shirts, shoes, blankets, textiles, soft goods, furniture,desk, work station, credenza, bookcase, file cabinet, chair, table, bed, donation, pet supplies, eyeglasses, jewelry, pet food, sporting goods, toiletries, toys, games, school supplies, musical instruments"},
        {"name":"Construction Waste","alternate": "proper disposal, ashes, asphalt, bone meal, brick, cement, ceramic tile, chalk, charcoal, clay, concrete, cork, earth, fiberglass insultion, fines, glass, granite, gravel, gypsum, lime, limestone, mortar, mud, pebbles, pumice, quartz, rock, sand, sewage sludge, sheetrock, slag, slate, sludge, soap, soil, sandy loam, stone, wax, wood, metal, fence, scrap, dirt, clean dirt, fill"},
        {"name":"Computers","alternate":"household goods, electronics, CPU, monitor, printer, mouse, peripheral, scanner, computer equipment, software, hardware, personal computer, laptop, keyboard, tower, toner cartridge, ink cartridge, fax machine, typewriter, copier, cell phone, calculator, speakers, telephone, CD, DVD, cables, tablets, ipad, e reader, hard drive, circuit board, power cord, power supply, CD ROM, USB drive, cords, laser ink jet, hub, modem, router, smart phone, iphone, andriod phone, video games, radio, stereo, record player, battery back-up, power adapter, remote, surge protector, cameras, video camera, tape drive"},
        {"name":"Drink Pouches & Other Packaging","alternate":"household goods, Capri Sun, juice box, drink box"},
        {"name":"Fire Extinguishers","alternate":"household hazardous waste, HHW"},
        {"name":"Flags","alternate":"proper disposal, tangler, U.S. flag, flag disposal"},
        {"name":"Flares","alternate":"proper disposal, road flares"},
        {"name":"Food","alternate":"compost"},
        {"name":"Fryer Oil ","alternate":"peanut oil, turkey fryer, deep fryer oil, oil disposal, cooking oil, grease"},
        {"name":"Furniture","alternate":"household goods, desk, chair, bookcase, "},
        {"name":"Hazardous Waste (oil-based paint, pesticides)","alternate":"household hazardous waste, HHW, paint, pesticides, gasoline, stains, insecticide, fertilizer, hazardous materials, garage clean-out, dangerous waste, flammable, turpentine, oil, 55-gallon drum, gas oil mix, paint cans, helium, balloon tanks"},
        {"name":"Latex Paint","alternate":"household hazardous waste, HHW, proper disposal, paint cans, water paint, paint, paint cans, "},
        {"name":"Mattresses","alternate":"household goods"},
        {"name":"Medicine & Needles","alternate":"household hazardous waste, HHW, proper disposal, prescription, drugs, meds, drop box, medications, syringes, self injection, sharps"},
        {"name":"Mercury & Fluorescent Bulbs","alternate":"household hazardous waste, HHW, thermometer, thermostat, silver mercury, tube bulbs, CFL, compact fluorescent bulb, squiggly bulb, LED, incandescent, light bulb, fluorescent tube, halogen, twisty bulb, bulbs"},
        {"name":"Motor Oil ","alternate":"household hazardous waste, HHW, engine oil, oil, car oil"},
        {"name":"Office Equipment","alternate":"electronics, household goods, "},
        {"name":"Paper Shredding","alternate":"household goods, shredded paper, document destuction, shred days, shredding, personal documents, documents, personal papers, city shred day, paper drop-off"},
        {"name":"Phone Books","alternate":"curbside recycling"},
        {"name":"Plastic Bags","alternate":"tangler, proper disposal,  recycling bags, plastic film, Ziplock bags, grocery bags, blue bags, shopping bags, plastic"},
        {"name":"Propane Tanks","alternate":"household hazardous waste, HHW, grill tanks, small tanks, propane, 20# propane, tanks, helium, balloon tanks"},
        {"name":"Smoke Detectors","alternate":"proper disposal"},
        {"name":"Styrofoam","alternate":"household goods, packing peanuts, food trays, Styrofoam blocks, polystyrene"},
        {"name":"Televisions & Electronics","alternate":"electronics, TV, flat screen, CRT, tube TV, LCD, LED, smart TV, VCR, Blu ray, DVD, cable box, dish, CPU, monitor, printer, mouse, peripheral, scanner, computer equipment, software, hardware, personal computer, laptop, keyboard, tower, toner cartridge, ink cartridge, fax machine, typewriter, copier, cell phone, calculator, speakers, telephone"},
        {"name":"Tires","alternate":"proper disposal, scrap tires, bike tires, bicycle tires, truck tires, tire pick-up, tire law, tire rims, motorcycle tires"},
        {"name":"Tools & (usable) Building Materials","alternate":"proper disposal, wood, lumber, deconstruction, hand tools, power tools, lawn mower, kitchen cabinets, donate building, Habitat"},
        {"name":"Trash","alternate":"proper disposal, garbage, waste, landfill, trash cans, transfer station, garbage cans, trash truck, Cleveland trash, dump, rubbish, solid waste, debris, mixed waste, disposal, scrap"},
        {"name":"Yard Waste","alternate":"household goods, compost, grass, trees, bushes, leaves, brush, branches, compost, logs, organics, prunings, hay, straw, stumps, limbs, prunings, yard trimmings, mulch, wood chips, dirt, manure, kraft bag"}
    ];
};