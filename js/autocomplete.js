'use strict';
let inputTimeOUt;
let isOpen = true;

const keyCodes = {
    ENTER: 13,
    ESCAPE: 27
};

const resizeDropDown = function () {
    const input = $('.full-search');
    const position = input.position();


    $('#dropdown-container').css({
        top: position.top + 32,
        left: position.left,
        width: input.width(),
        position: 'fixed'
    });

};

/**
 * Change default bar with placeholder to search-bar
 */
const changeBar = function () {
    $('.hide').removeClass('hide');
    $(this).addClass('hide');
    resizeDropDown();
};

const clearDropDown = function () {
    $('#dropdown').html('');
};

const closeDropDown = function () {
    if(!isOpen) {
        return;
    }
    $('#dropdown').hide();
    isOpen = false;
};

const showDropDown = function () {
    if(isOpen) {
        return;
    }
    $('#dropdown').show();
    isOpen = true;
};

const fillDropDown = function (data) {
    clearDropDown();
    const dropdown = $('#dropdown');
    data.forEach(function(item) {
        dropdown.append('<li>' + item +'</li>');
    });
};

const selectItem = function (e) {
    $('.full-search').val(e.target.innerText);
    closeDropDown();
};

const selectFirstItem = function () {
    const val = $('#dropdown li:first-child')[0].innerText;
    $('.full-search').val(val);
    closeDropDown();
};

const clearResult = function () {
    $('.full-search').val('');
    closeDropDown();
};

/**
 * Search category by alternate worlds
 */
const searchCategory = function () {
    if (!this.value) {
        return closeDropDown();
    }
    const data = getData();
    const matches = [];
    const val = new RegExp(this.value, "i");

    data.forEach(function (item) {
        if (item.alternate.search(val) !== -1)
            matches.push(item.name);
    });

    showDropDown();
    fillDropDown(matches);
};

/**
 * handle input of search
 * @param e
 */
const searchSection = function (e) {
    if(e.which === keyCodes.ESCAPE) {
        return closeDropDown();
    }

    if(e.which === keyCodes.ENTER) {
        return selectFirstItem();
    }

    clearTimeout(inputTimeOUt);
    inputTimeOUt = setTimeout(searchCategory.bind(this), 100);
};

/**
 * Add container with dropdown list to wrapper element
 */
const initDropDown = function () {
    $('.wrapper').after('<div id="dropdown-container" class="shadow-dropdown"></div>');
    $('#dropdown-container').append('<ul id="dropdown" class="ul-dropdown"></ul>');
    $('#dropdown').on('click', selectItem);
    closeDropDown();
};

/**
 * Add variable placeonholder part on static searchbar
 */
const randomPlaceholder = function () {
    const placeholders = ['Plastic', 'Glass', 'Cartons', 'Paper'];
    $('.section-search').val(placeholders[Math.floor(Math.random() * (placeholders.length - 1))]);
};

const searchIconActive = function() {
    const searchIcon = $('.icon-wrapper');
    if (searchIcon.hasClass('bg-input')) {
        searchIcon
            .removeClass('bg-input')
            .addClass('bg-active');
    } else {
        searchIcon
            .removeClass('bg-active')
            .addClass('bg-input');
    }
};

$(document).ready(function () {

    randomPlaceholder();
    $('.section').click(changeBar);
    $('.close-wrapper').click(clearResult);

    $('.full-search')
        .keyup(searchSection)
        .focus(searchIconActive)
        .focusout(function() {
            searchIconActive()
            setTimeout(closeDropDown, 100);
        });


    $(window).on('resize.input-dropdown', resizeDropDown);

    initDropDown();
});

